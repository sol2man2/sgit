package org.moringa.oleifera.sgit;

import java.io.File
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.ReflogCommand
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revplot.PlotCommitList
import org.eclipse.jgit.revplot.PlotLane
import org.eclipse.jgit.revplot.PlotWalk
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.treewalk.TreeWalk
import java.io.PrintStream
import java.io.FileOutputStream
import sun.management.HotspotClassLoading
import org.eclipse.jgit.diff.DiffAlgorithm

class AppMain {

}

object AppMain {

  val serverName = "localhost"

  val dbServer = "localhost"
  val dbName = "sgit"

  def main(args: Array[String]): Unit = {
//    trial1

    exec(makeRepo)
//    exec(makeListOfWallRepos)
    //    tutorial
  }

  def trial1 {
    val myersDiffAlgo = DiffAlgorithm.SupportedAlgorithm.valueOf("MYERS")
    val diffAlgorithm = DiffAlgorithm.getAlgorithm(myersDiffAlgo)
  }

//  def makeRepo = "C:\\zGit\\huk\\.git\\"
//  def makeRepo = "C:\\zGit\\zeta\\.git\\"
//  def makeRepo = "C:\\zGit\\zeta1\\zeta\\.git\\"
//  def makeRepo = "C:\\zSource\\lg\\sam\\.git\\"
  def makeRepo = "C:\\zLg\\mc\\LG_apps_master\\AuHome\\.git"
  

  def exec(repoName: String) {
    //    val repositoryName = "C:\\zGit\\zeta1\\zeta\\.git\\"
    //    val repositoryName = "C:\\zGit\\huk\\.git\\"
    //    val repositoryName = "C:\\zSource\\lg\\AuHome\\.git\\"
//    val repositoryName = "C:\\zSource\\lg\\sam\\.git\\"
    //    val repositoryName = "C:\\zGit\\huk\\.git\\"

    val hotSpotInSourceCode = new HotSpotInSourceCode(dbServer, dbName, repoName)
    hotSpotInSourceCode.analyze
  }

  def makeListOfWallRepos: List[String] = {
    val appFolderName = "/home/nature.song/lggit/he/wall/app/"
    val serviceFolderName = "/home/nature.song/lggit/he/wall/service/"
    val moduleFolderName = "/home/nature.song/lggit/he/wall/module/"

    //    val repositoryName = baseFolderName + "com.webos.app.skype/"
    val gitFolerName = "/.git"

    val repositoryList = List(
      //        appFolderName + "FiOSTV" + gitFolerName,		--No Head
      //        appFolderName + "Vudu" + gitFolerName,		--No Head
      //        appFolderName + "com.lge.app.cineplex" + gitFolerName,	--No Head
      appFolderName + "com.lge.app.mycontents" + gitFolerName,
      //        appFolderName + "com.lge.netflix" + gitFolerName,		--No Head
      //        appFolderName + "com.lge.showcase.boardgameapps" + gitFolerName,	--No Head
      //        appFolderName + "com.lge.showcase.boardtodays" + gitFolerName,	--No Head
      appFolderName + "com.lge.showcase.qmlboard" + gitFolerName,
      appFolderName + "com.webos.app.brandshop" + gitFolerName,
      appFolderName + "com.webos.app.channelsetting" + gitFolerName, //	with Lost
      //        appFolderName + "com.webos.app.commercial.livetv" + gitFolerName,	--No Head
      appFolderName + "com.webos.app.discovery" + gitFolerName,
      appFolderName + "com.webos.app.fingergesture" + gitFolerName,
      appFolderName + "com.webos.app.installation" + gitFolerName, //	with Lost
      appFolderName + "com.webos.app.membership" + gitFolerName,
      appFolderName + "com.webos.app.miracast" + gitFolerName, //	with Lost
      appFolderName + "com.webos.app.quickmemo" + gitFolerName,
      appFolderName + "com.webos.app.skype" + gitFolerName,
      appFolderName + "com.webos.app.softwareupdate" + gitFolerName, //	with Lost
      appFolderName + "com.webos.app.tvcomponent" + gitFolerName, //	with Lost
      appFolderName + "com.webos.app.tvhotkey" + gitFolerName,
      appFolderName + "com.webos.app.urcu" + gitFolerName,
      appFolderName + "input-analog-app-plugin" + gitFolerName, //	with Lost
      appFolderName + "netcast-webapps" + gitFolerName,

      serviceFolderName + "AJService" + gitFolerName,
      //        serviceFolderName + "AllJoyn-Daemon" + gitFolerName,		--No Head
      //        serviceFolderName + "Photoservice" + gitFolerName,		--No Head
      //        serviceFolderName + "RemoteApp" + gitFolerName,		--No Head
      //        serviceFolderName + "SecurityManager" + gitFolerName,	--No Head
      serviceFolderName + "WirelessHRZ" + gitFolerName,
      serviceFolderName + "acr" + gitFolerName,
      serviceFolderName + "admanager" + gitFolerName,
      serviceFolderName + "ajservice" + gitFolerName,
      serviceFolderName + "bluetooth-manager" + gitFolerName,
      serviceFolderName + "camera" + gitFolerName,
      serviceFolderName + "camera-pipeline" + gitFolerName,
      serviceFolderName + "camera-service" + gitFolerName,
      serviceFolderName + "cbox" + gitFolerName,
      serviceFolderName + "com.webos.service.cloudapiwrapper" + gitFolerName,
      serviceFolderName + "com.webos.service.dial" + gitFolerName,
      serviceFolderName + "com.webos.service.emulremocon" + gitFolerName,
      serviceFolderName + "com.webos.service.membership" + gitFolerName,
      serviceFolderName + "com.webos.service.nlpmanager" + gitFolerName,
      //        serviceFolderName + "com.webos.service.nms" + gitFolerName,	--No Head
      serviceFolderName + "com.webos.service.quickmemo" + gitFolerName,
      serviceFolderName + "com.webos.service.remotemanagerservice" + gitFolerName,
      serviceFolderName + "com.webos.service.smart-demo-kit" + gitFolerName,
      //        serviceFolderName + "commercial.gm" + gitFolerName,		--No Head
      //        serviceFolderName + "commercial.im" + gitFolerName,			--No Head
      serviceFolderName + "dmr" + gitFolerName,
      serviceFolderName + "eim" + gitFolerName,
      serviceFolderName + "gm" + gitFolerName,
      serviceFolderName + "iepg" + gitFolerName,
      serviceFolderName + "im" + gitFolerName,
      serviceFolderName + "irdbmanager" + gitFolerName,
      serviceFolderName + "lgskype" + gitFolerName,
      serviceFolderName + "miracast" + gitFolerName,
      serviceFolderName + "mrcu" + gitFolerName,
      serviceFolderName + "nlp" + gitFolerName,
      serviceFolderName + "oss" + gitFolerName,
      serviceFolderName + "pbs" + gitFolerName,
      serviceFolderName + "pbsw" + gitFolerName,
      //        serviceFolderName + "pdevmanager" + gitFolerName,		--No Head
      serviceFolderName + "photoservice" + gitFolerName,
      serviceFolderName + "physical-device-manager" + gitFolerName,
      serviceFolderName + "prs" + gitFolerName,
      serviceFolderName + "push" + gitFolerName,
      //        serviceFolderName + "racagent" + gitFolerName,		--No Head
      serviceFolderName + "rovi" + gitFolerName,
      serviceFolderName + "sbm" + gitFolerName,
      serviceFolderName + "sdx" + gitFolerName,
      serviceFolderName + "smartservice" + gitFolerName,
      serviceFolderName + "streamingserver" + gitFolerName,
      serviceFolderName + "testagent" + gitFolerName,
      serviceFolderName + "tnm" + gitFolerName,
      serviceFolderName + "tts" + gitFolerName,
      serviceFolderName + "tvpower-manager" + gitFolerName,
      serviceFolderName + "tvpowerd" + gitFolerName,
      serviceFolderName + "upnp" + gitFolerName,
      serviceFolderName + "vtg" + gitFolerName,
      //        serviceFolderName + "vtv" + gitFolerName,		--No Head
      //        serviceFolderName + "wall.service" + gitFolerName,		--No Head

      //        moduleFolderName + "GINGAJ" + gitFolerName,		--No Head
      moduleFolderName + "camera-plugin" + gitFolerName,
      moduleFolderName + "hls-player" + gitFolerName,
      moduleFolderName + "htvbrowser_old" + gitFolerName,
      moduleFolderName + "libsnapshot-boot" + gitFolerName,
      moduleFolderName + "mhegics-player" + gitFolerName //        moduleFolderName + "sdk" + gitFolerName				//--No Head
      )
    repositoryList
  }

  def exec(repositoryList: List[String]) {
    repositoryList.foreach(rName => {
      println("repository name: " + rName)
      new HotSpotInSourceCode(dbServer, dbName, rName).analyze
    })
  }

  def tutorial {
    //    val repository = getRepository("C:\\zProject\\zAndroid\\AuHome\\.git\\")
    val repository = getRepository("C:\\zGIt\\first\\.git\\")

    //    aboutHeadCommitTree(repository)
    //    aboutRef(repository)
    //    aboutReflogCommand(repository)
    //    aboutDiffCommand(repository)
    aboutLog(repository)
    //        aboutWalk(repository)
    //    aboutResolveRef(repository)
    //    aboutConfig(repository)
    //        aboutTree(repository)
    //    aboutRevTree(repository)
    //    aboutRemote(repository)
    //    aboutBranch(repository)
    //        aboutPlotWalk(repository)
    //    checkTree(repository)

    //        aboutAddCommand(repository)

    repository.close
  }

  def getRepository(repositoryName: String) = {
    val builder = new FileRepositoryBuilder();
    builder.setGitDir(new File(repositoryName)).readEnvironment().findGitDir().build()
  }

  def aboutDiffCommand(repository: Repository) {

    val git = new Git(repository)
    //    val diffCommand = new DiffCommand(repository)
    val revWalk = new RevWalk(repository)
    val logs = git.log.all.call

    val iter = logs.iterator

    while (iter.hasNext) {
      val log = iter.next
    }

    val commit = revWalk.parseCommit(repository.resolve(Constants.HEAD))

  }

  def aboutReflogCommand(repository: Repository) {
    val reflogCommand = new ReflogCommand(repository)
    val collection = reflogCommand.call
    val iter = collection.iterator

    while (iter.hasNext) {
      val reflogEntry = iter.next
      println("==========================================")
      println("who: " + reflogEntry.getWho)
      println("Comment: " + reflogEntry.getComment)
      println("NewId: " + reflogEntry.getNewId)
      println("OldId: " + reflogEntry.getOldId)
    }
  }

  def aboutPlotWalk(repository: Repository) {
    val plotWalk = new PlotWalk(repository);
    val rootId = repository.resolve("refs/heads/master");
    val rootCommit = plotWalk.parseCommit(rootId);
    plotWalk.markStart(rootCommit)

    val plotCommitList = new PlotCommitList[PlotLane]()
    plotCommitList.source(plotWalk)
    plotCommitList.fillTo(Int.MaxValue)

    println("Printing children of commit " + rootCommit)
    val iter = plotWalk.iterator

    while (iter.hasNext) {
      val commit = iter.next
      println("Child: " + commit)
    }

    println("Printing with next()");
    println("next: " + plotWalk.next());

  }

  def aboutBranch(repository: Repository) {
    val refs = new Git(repository).branchList.call

    val iter = refs.iterator
    while (iter.hasNext) {
      val ref = iter.next
      println("ref: " + ref)
    }
  }

  def checkTree(repository: Repository) {
    val revWalk = new RevWalk(repository)
    val commit = revWalk.parseCommit(repository.resolve(Constants.HEAD))
    val revTree = revWalk.parseTree(commit.getTree.getId)
    val tree = commit.getTree

    println("revTree: " + revTree)
    println("tree: " + tree)
  }

  def aboutRemote(repository: Repository) {
    val config = repository.getConfig
    val remotes = config.getSubsections("remote");

    val iter = remotes.iterator
    while (iter.hasNext) {
      val remoteName = iter.next
      println("remoteName: " + remoteName)
    }
  }

  def aboutRevTree(repository: Repository) {
    val revWalk = new RevWalk(repository)
    val commit = revWalk.parseCommit(repository.resolve(Constants.HEAD))
    val revTree = revWalk.parseTree(commit.getTree.getId)

    println("tree: " + revTree)
    repository.open(revTree.getId()).copyTo(System.out)
  }

  def aboutTree(repository: Repository) {
    val revWalk = new RevWalk(repository)
    val commit = revWalk.parseCommit(repository.resolve(Constants.HEAD))
    val tree = commit.getTree
    val treeWalk = new TreeWalk(repository)
    treeWalk.addTree(tree)
    treeWalk.setRecursive(true)

    while (treeWalk.next) {
      val objId = treeWalk.getObjectId(0)
      val objLoader = repository.open(objId)
      println("\t-----------------------")

      objLoader.copyTo(System.out)
    }
    //    treeWalk.enterSubtree()
  }

  def aboutConfig(repository: Repository) {
    val config = repository.getConfig()
    val name = config.getString("user", null, "name")
    val email = config.getString("user", null, "email")
    if (name != null && email != null) {
      println("name: " + name + ", email: " + email)
    }
  }

  def aboutResolveRef(repository: Repository) {
    val objId = repository.resolve("HEAD")
    println("ObjectId of HEAD: " + objId)

    println("ObjectId of HEAD: " + repository.resolve("HEAD^1"))
  }

  def aboutWalk(repository: Repository) {
    val refHead = repository.getRef("refs/heads/master")
    val walk = new RevWalk(repository);

    val commit = walk.parseCommit(refHead.getObjectId())
    println("head commit: " + commit)

    walk.markStart(commit)
    val iter = walk.iterator
    while (iter.hasNext) {
      val commit = iter.next
      println("commit: " + commit + ", " + commit.getShortMessage)
    }
  }

  //  def about1Log(repository: Repository) {
  //    val git = new Git(repository)
  //    val log = git.log
  //    log.
  //    val iterables = git.log.all.call()
  //    println("iterables: " + iterables)
  //
  //  }

  def aboutLog(repository: Repository) {
    val git = new Git(repository)
    val iterables = git.log.all.call()
    println("iterables: " + iterables)

    val iters = iterables.iterator()

    var idx = 0

    while (iters.hasNext) {
      idx += 1
      val commit = iters.next
      displayCommitInfo(commit)
      //      println("[" + idx + ":" + "?" + "]: " + commit)
    }
  }

  def displayCommitInfo(commit: RevCommit) {
    println("//---------------------------------------------")
    //    println("AuthorId: " + commit.getAuthorIdent)
    //    println("CommitterId: " + commit.getCommitterIdent)
    //    println("Time: " + commit.getCommitTime)
    displayParents(commit)
    //    println("FullMessage: " + commit.getFullMessage)
    println("ShortMessage: " + commit.getShortMessage)
    //    println("FooterLines: " + commit.getFooterLines)
    //    println("Tree: " + commit.getTree)
    //    println("Type: " + commit.getType)
  }

  def displayParents(commit: RevCommit) {

    println("tree name: " + commit.getTree.getName)
    def displayParnent(idx: Int, commit: RevCommit) {
      println("[" + idx + "]" + commit)
    }

    for (idx <- (1 to commit.getParentCount)) {
      displayParnent(idx, commit.getParent(idx - 1))
    }
  }

  def aboutAddCommand(repository: Repository) {

    val git = new Git(repository)
    val addCommandFirst = git.add();
    println("addCommandFirst: " + addCommandFirst)

    val addCommandSecond = addCommandFirst.addFilepattern("someDirectory").call();
    println("addCommandSecond: " + addCommandSecond)
  }

  def aboutRef(repository: Repository) {
    val refHead = repository.getRef("refs/heads/master")
    println("type of refHead: " + refHead.getClass)
    println("refHead: " + refHead)
  }

  def aboutHeadCommitTree(repository: Repository) {
    val head: ObjectId = repository.resolve("HEAD")
    println("head: " + head)

    val walk = new RevWalk(repository)
    println("walk: " + walk)

    val commit = walk.parseCommit(head)
    println("commit: " + commit)

    val tree = walk.parseTree(head)
    println("tree: " + tree)
  }
}
