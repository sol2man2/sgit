package org.moringa.oleifera.sgit

import java.io.File
import java.sql.Connection
import scala.collection.mutable.HashMap
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.moringa.oleifera.sgit.model.DBConnection
import org.moringa.oleifera.sgit.model.SgitChangeType
import org.moringa.oleifera.sgit.model.DataAgent
import org.moringa.oleifera.sgit.model.SgitAnalysis
import org.moringa.oleifera.sgit.model.SgitFunctionComplexity
import org.moringa.oleifera.sgit.model.SgitCommit
import org.moringa.oleifera.sgit.model.SgitDiffEntry
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.Date

class HotSpotInSourceCode(dbServer: String, dbName: String, repositoryName: String) {

  val builder = new FileRepositoryBuilder
  val repository = builder.setGitDir(new File(repositoryName)).readEnvironment().findGitDir().build()

  var dbConnection: Connection = null
  val dataAgent: DataAgent = new DataAgent(dbServer, dbName)

  var changeTypeMap: HashMap[String, Int] = null
  var pathMap: HashMap[String, Int] = null
  var fileMap: HashMap[String, Int] = null

  def analyze {

    initMemCache

    isValidRepository match {
      case false => {
        println("repository corrupt")
        return
      }
      case true =>
    }

    val analysis = getSgitAnalysis
    printSgitAnalysis(analysis)
    makeSgitAnalysisCSV(analysis)

    //    analysis.getSgitCommits.foreach(commit => {
    //      println(commit.getObjectId)
    //      commit.getDiffEntries.foreach(diffEntry => {
    //        println("\t" + diffEntry.getChangeType + ", " + diffEntry.getPath + diffEntry.getFile)
    //        diffEntry.getSgitFunctionComplexities.foreach(functionComplexity => {
    //          println("\t\t" + functionComplexity.getFunction + "~" + functionComplexity.getPrevComplexity + ", " + functionComplexity.getNextComplexity)
    //        })
    //      })
    //    })

    //    val result = insertSgitAnalysis(analysis)
    //    println("result: " + result)

    dbConnection = DBConnection.connectDB(dbServer + "/" + dbName)
    dbConnection.close
  }

  def makeSgitAnalysisCSV(analysis: SgitAnalysis) {
    var out_file = new java.io.FileOutputStream("analysis.csv")
    var out_stream = new java.io.PrintStream(out_file)

    val sf: DateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    
    out_stream.println("ObjectId", "AuthorId", "Date", "PrevFile", "NextFile", "Added", "Modified", "Deleted", "Untouched")
    analysis.getSgitCommits foreach (commit => {
      val diffEntries = commit.getDiffEntries
      diffEntries foreach (diffEntry => {
        val sgitFileSLOC = diffEntry.getSgitFileSLOC

        out_stream.print(commit.getObjectId)
        out_stream.print(", ")
        out_stream.print(commit.getAuthorId.getName)
        out_stream.print(", ")
        out_stream.print(sf.format(commit.getAuthorId.getWhen))
        out_stream.print(", ")

        out_stream.print(sgitFileSLOC.getPrecFile)
        out_stream.print(", ")
        out_stream.print(sgitFileSLOC.getNextFile)
        out_stream.print(", ")
        
        out_stream.print(sgitFileSLOC.getAdded)
        out_stream.print(", ")
        
        out_stream.print(sgitFileSLOC.getModified)
        out_stream.print(", ")

        out_stream.print(sgitFileSLOC.getDeleted)
        out_stream.print(", ")

        out_stream.print(sgitFileSLOC.getUntouched)
        out_stream.println
      })
    })
    out_stream.close
  }

  def printSgitAnalysis(analysis: SgitAnalysis) {
    var out_file = new java.io.FileOutputStream("analysis.result")
    var out_stream = new java.io.PrintStream(out_file)
    analysis.getSgitCommits foreach (commit => {
      out_stream.println("commit infor: " + commit.getObjectId + ": " + commit.getAuthorId.getName)
      val diffEntries = commit.getDiffEntries
      diffEntries foreach (diffEntry => {
        out_stream.println("\tdiffentry: " + diffEntry.getFile + ":" + diffEntry.getSgitFileSLOC)
      })
    })
    out_stream.close
  }

  def insertSgitAnalysis(sgitAnalysis: SgitAnalysis): Boolean = {
    val result = dataAgent.insertSgitAnalysis(sgitAnalysis.getRepositoryName, sgitAnalysis.getDate)

    def insertSgitCommit(sgitAnalysisKey: Int, sgitCommit: SgitCommit): Boolean = {
      val result = dataAgent.insertSgitCommit(sgitAnalysisKey, sgitCommit)

      def insertSgitDiffEntry(sgitCommitKey: Int, sgitDiffEntry: SgitDiffEntry): Boolean = {
        println("\tsgitDiffEntry.getChangeType#: " + sgitDiffEntry.getChangeType)
        val changeTypeKey = getChangeType(sgitDiffEntry.getChangeType).get
        val pathKey = getPathName(sgitDiffEntry.getPath).get
        val fileKey = getFileName(sgitDiffEntry.getFile).get

        val result = dataAgent.insertSgitDiffEntry(sgitCommitKey, changeTypeKey, pathKey, fileKey)

        def insertSgitFunctionComplexity(sgitDiffEntryKey: Int, sgitFunctionComplexity: SgitFunctionComplexity): Boolean = {
          val result = dataAgent.insertSgitFunctionComplexity(sgitDiffEntryKey, sgitFunctionComplexity.getFunction,
            sgitFunctionComplexity.getPrevComplexity, sgitFunctionComplexity.getNextComplexity)

          result match {
            case Some(functionComplexityKey) => true
            case None => false
          }
        }

        result match {
          case Some(diffEntryKey) => sgitDiffEntry.getFunctionComplexities.forall(insertSgitFunctionComplexity(diffEntryKey, _))
          case None => false
        }
      }

      result match {
        case Some(commitKey) => sgitCommit.getDiffEntries.forall(insertSgitDiffEntry(commitKey, _))
        case None => false
      }
    }

    result match {
      case Some(analysisKey) => sgitAnalysis.getSgitCommits.forall(insertSgitCommit(analysisKey, _))
      case None => false
    }
  }

  def initMemCache {
    changeTypeMap = dataAgent.selectSgitChangeType
    pathMap = dataAgent.selectSgitPath
    fileMap = dataAgent.selectSgitFile
  }

  def getPathName(pathName: String): Option[Int] = {
    pathMap.contains(pathName) match {
      case true => pathMap.get(pathName)
      case false => {
        val result = dataAgent.insertPathName(pathName)

        result match {
          case Some(pKey) => {
            pathMap.put(pathName, result.get)
            result
          }
          case None => None
        }
      }
    }
  }

  def getFileName(fileName: String): Option[Int] = {
    fileMap.contains(fileName) match {
      case true => fileMap.get(fileName)
      case false => {
        val result = dataAgent.insertFileName(fileName)

        result match {
          case Some(pKey) => {
            fileMap.put(fileName, result.get)
            result
          }
          case None => None
        }
      }
    }
  }

  def getChangeType(changeTypeConst: Int): Option[Int] = {
    val changeTypeStr = SgitChangeType.getSgitChangeTypeStrFromType(changeTypeConst)

    changeTypeMap.contains(changeTypeStr) match {
      case true => changeTypeMap.get(changeTypeStr)
      case false => {
        val result = dataAgent.insertChangeType(changeTypeStr)
        result match {
          case Some(pKey) => {
            changeTypeMap.put(changeTypeStr, result.get)
            result
          }
          case None => None
        }
      }
    }
  }

  def isValidRepository: Boolean = {
    val git = new Git(repository)

    try {
      val revWalk = new RevWalk(repository)
      val commits = git.log.all.call
    } catch {
      case e: Exception => {
        e.printStackTrace()
        return false
      }
    }
    true
  }

  def getSgitAnalysis: SgitAnalysis = {
    val sgitAnalysis = SgitAnalysis.getSgitAnalysis(repository)

    sgitAnalysis
  }

}