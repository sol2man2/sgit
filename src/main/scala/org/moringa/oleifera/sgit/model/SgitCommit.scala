package org.moringa.oleifera.sgit.model

import org.eclipse.jgit.lib.PersonIdent
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.revwalk.RevCommit
import scala.collection.mutable.ListBuffer
import org.eclipse.jgit.lib.Repository

object SgitCommit {

  def getSgitCommits(repository: Repository, commits: List[RevCommit]): List[SgitCommit] = {
    val sgitCommits = commits.map(commit => {
println("commit :" + commit)
      SgitCommit.getSgitCommit(repository, commit) match {
        case one: List[SgitCommit] if (one.length == 1) => one.head
//        case two: List[SgitCommit] if (two.length == 2) => Nil
        case _ => Nil
      }
    }).filter(_ != Nil)
    
    sgitCommits.asInstanceOf[List[SgitCommit]]
  }

  def getSgitCommit(repository: Repository, commit: RevCommit): List[SgitCommit] = {

    var sgitCLB = new ListBuffer[SgitCommit]
    val parents = commit.getParents.toList

    parents.map( parent => {
      val diffEntries = SgitDiffEntry.getSgitDiffEntries(repository, commit, parent)

      sgitCLB += new SgitCommit(commit.getId, commit.getFullMessage, commit.getAuthorIdent, commit.getCommitterIdent, diffEntries)
    })

    sgitCLB.toList
  }
}

class SgitCommit(objectId: ObjectId, msg: String, authorId: PersonIdent, committerId: PersonIdent, diffEntries: List[SgitDiffEntry]) {

  def getObjectId = objectId
  def getMsg = msg
  def getAuthorId = authorId
  def getCommitterId = committerId
  def getDiffEntries = diffEntries
  
  override def toString = {
    val sb = new StringBuilder
    sb.append("ObjectId: ").append(objectId).
      append(", Msg: ").append(msg).
      append(", AuthorId: ").append(authorId).
      append(", CommitterId: ").append(committerId).append("\n").
      append(", SgitDiffEntry: ").append(diffEntries)
    sb.toString
  }
}
