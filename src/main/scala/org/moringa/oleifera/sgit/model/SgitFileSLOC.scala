package org.moringa.oleifera.sgit.model

import scala.sys.process.stringToProcess
import scala.collection.mutable.HashMap
import java.io.File

object SgitFileSLOC {

  def execFileSLOC(basePath: String, prevPath: String, nextPath: String): String = {

    val resultFileName = "outfile_diff_results.csv"

    val cmd: String = "ucc -d -dir " +
      "." + File.separator + basePath + File.separator + prevPath + " " +
      "." + File.separator + basePath + File.separator + nextPath + " " +
      "* -outdir base"
//      "*.h *.hpp *.c *.cpp -outdir base"
      

    println("cmd: " + cmd)
      
    val result = cmd !

    basePath + File.separator + resultFileName
  }

  def getSgitFileSLOCs(basePath: String, prevPath: String, nextPath: String,
    slocFileName: String, prevUdbFile: String, nextUdbFile: String): List[SgitFileSLOC] = {

    val mapOld = new HashMap[String, SgitFileSLOC]
    val mapNew = new HashMap[String, SgitFileSLOC]

    val source = scala.io.Source.fromFile(slocFileName)
    val listSloc = source.getLines.toList.drop(8).takeWhile(_ != "").map(getSgitFileSLOC(basePath, prevPath, nextPath, _))

    listSloc.foreach(elem => {
      println("DIFF[" + elem.getPrecFile + ":" + elem.getNextFile + "]" + 
        elem.getAdded + "/" + elem.getModified + "/" + elem.getDeleted + "/" + elem.getUntouched)
    })

    listSloc
  }

  def getSgitFileSLOC(basePath: String, prevPath: String, nextPath: String, diffLines: String): SgitFileSLOC = {
    val diff = diffLines.split(",")
    val add: Int = diff(0).toInt
    val mod: Int = diff(2).toInt
    val del: Int = diff(1).toInt
    val untouch: Int = diff(3).toInt
    val changeType = SgitChangeType.getSgitChangeTypeFromStr(diff(4))

    val prevBase: String = basePath + File.separator + prevPath + File.separator
    val nextBase: String = basePath + File.separator + nextPath + File.separator

    val prevFile = diff(6).tail.init
    val nextFile = diff(7).tail.init

    def getPath(path: String, file: String): String = {
      file.lastIndexOf(path) match {
        case -1 => null
        case index => file.splitAt(index + path.length)._2
      }
    }

    val prev = getPath(prevBase, prevFile)
    val next = getPath(nextBase, nextFile)

    new SgitFileSLOC(prev, next, changeType, add, mod, del, untouch, null)
  }
}

class SgitFileSLOC(prevFile: String, nextFile: String, changeType: SgitChangeType.Value, added: Int, modified: Int, deleted: Int, untouched: Int, functionComplexities: List[SgitFunctionComplexity]) {
  //add: Int, mod: Int, del: Int, untouch: Int, changeType: String, file: String
  def getPrecFile = prevFile
  def getNextFile = nextFile
  def getSgitChangeType = changeType
  def getAdded = added
  def getModified = modified
  def getDeleted = deleted
  def getUntouched = untouched
  def getFunctionComplexities = functionComplexities
  
  override def toString = {
    val sb = new StringBuilder
    sb.append("PrevFile: ").append(prevFile).
      append(", NextFile: ").append(nextFile).
      append(", ChangeType: ").append(changeType).
      append(", Added: ").append(added).
      append(", modified: ").append(modified).
      append(", deleted: ").append(deleted).
      append(", Untouched: ").append(untouched)append("\n")
    sb.toString
  }
}
