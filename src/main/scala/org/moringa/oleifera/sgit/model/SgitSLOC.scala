package org.moringa.oleifera.sgit.model

object SgitSLOC {
  def getSgitSLOC: SgitSLOC = null
}

class SgitSLOC(deleted: Int, changed: Int, added: Int) {
  def getDeleted = deleted
  def getChanged = changed
  def getAdded = added
}
