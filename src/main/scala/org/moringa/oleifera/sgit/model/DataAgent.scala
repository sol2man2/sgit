package org.moringa.oleifera.sgit.model


import java.sql.Connection
import scala.collection.mutable.HashMap
import java.util.Date
import org.specs2.internal.scalaz.std.effect.sql.preparedStatement
import java.sql.Types


class DataAgent (serverName: String, dbName: String) {

  val cachePath = new HashMap[String, Int]
  val cachsFile = new HashMap[String, Int]

  val dbConn: Connection = DBConnection.connectDB(serverName + "/" + dbName)

  def insertChangeType(changeTypeStr: String): Option[Int] = {
    val key = Array("change_type_id")
    val query = "insert into change_type(change_type_name) values (?);"

    val ps = dbConn.prepareStatement(query, key)
    ps.setString(1, changeTypeStr)
    val result = ps.executeUpdate

    val rs = ps.getGeneratedKeys
    rs.next match {
      case true => Some(rs.getLong(key(0)).toInt)
      case false => None
    }
  }

  def insertPathName(pathName: String): Option[Int] = {
    val key = Array("path_id")
    val query = "insert into path(path_name) values (?);"

    val ps = dbConn.prepareStatement(query, key)
    ps.setString(1, pathName)
    val result = ps.executeUpdate

    val rs = ps.getGeneratedKeys
    rs.next match {
      case true => Some(rs.getLong(key(0)).toInt)
      case false => None
    }
  }

  def insertFileName(fileName: String): Option[Int] = {
    val key = Array("file_id")
    val query = "insert into file(file_name) values (?);"

    val ps = dbConn.prepareStatement(query, key)
    ps.setString(1, fileName)
    val result = ps.executeUpdate

    val rs = ps.getGeneratedKeys
    rs.next match {
      case true => Some(rs.getLong(key(0)).toInt)
      case false => None
    }
  }

  def insertSgitFunctionComplexity(sgitDiffEntryId: Int, functionName: String, prevComplexity: Option[Int], nextComplexity: Option[Int]): Option[Int] = {
    val key = Array("function_complexity_id")
    val query = "insert into function_complexity(diff_entry_id, function_name, previous_complexity, next_complexity) values(?, ?, ?, ?);"
      
    val ps = dbConn.prepareStatement(query, key)
    ps.setInt(1, sgitDiffEntryId)
    ps.setString(2, functionName)
    prevComplexity match {
      case Some(pc) => ps.setInt(3, pc)
      case None => ps.setNull(3, Types.INTEGER)
    }
    nextComplexity match {
      case Some(nc) => ps.setInt(4, nc)
      case None => ps.setNull(4, Types.INTEGER)
    }
    val result = ps.executeUpdate
    
    val rs = ps.getGeneratedKeys
    rs.next match {
      case true => Some(rs.getLong(key(0)).toInt)
      case false => None
    }
  }

  def insertSgitDiffEntry(sgitCommitId: Int, sgitChangeTypeKey: Int, sgitPathKey: Int, sgitFileKey: Int): Option[Int] = {
    val key = Array("diff_entry_id")
    val query = "insert into diff_entry(commit_id, change_type_id, path_id, file_id) values (?, ?, ?, ?);"

    val ps = dbConn.prepareStatement(query, key)
    ps.setInt(1, sgitCommitId)
    ps.setInt(2, sgitChangeTypeKey)
    ps.setInt(3, sgitPathKey)
    ps.setInt(4, sgitFileKey)
    val result = ps.executeUpdate

    val rs = ps.getGeneratedKeys
    rs.next match {
      case true => Some(rs.getLong(key(0)).toInt)
      case false => None
    }
  }

  def insertSgitCommitComplexity(sgitCommitId: Int, functionName: String, prevCc: Option[Int], nextCc: Option[Int]): Option[Int] = {
    val key = Array("commit_complexity_id")
    val query = "insert into commit_complexity(commit_id, function, previous_complexity, next_complexity) values (?, ?, ?, ?);"

    val ps = dbConn.prepareStatement(query, key)
    ps.setInt(1, sgitCommitId)
    ps.setString(2, functionName)
    prevCc match {
      case Some(cc) => ps.setInt(3, cc)
      case None => {
        ps.setNull(3, Types.INTEGER)
        println(functionName + ": none prev")
      }
    }
    nextCc match {
      case Some(cc) => ps.setInt(4, cc)
      case None => {
        ps.setNull(4, Types.INTEGER)
        println(functionName + ": none next")
      }
    }
    val result = ps.executeUpdate

    val rs = ps.getGeneratedKeys
    rs.next match {
      case true => Some(rs.getLong(key(0)).toInt)
      case false => None
    }
  }

  def insertSgitCommit(sgitAnalysisId: Int, sgitCommit: SgitCommit): Option[Int] = {
    def getOwnerString(owner: String): String = {
      owner.indexOf("@") match {
        case -1 => owner
        case index => owner.splitAt(index)._1
      }
    }
    val key = Array("commit_id")
    val query = "insert into commit(analysis_id, author, committer, commit_date, commit_object_id, commit_msg) values (?, ?, ?, ?, ?, ?);"

    val ps = dbConn.prepareStatement(query, key)
    ps.setInt(1, sgitAnalysisId)
    ps.setString(2, getOwnerString(sgitCommit.getAuthorId.getName))
    ps.setString(3, getOwnerString(sgitCommit.getCommitterId.getName))
    ps.setDate(4, new java.sql.Date(sgitCommit.getAuthorId.getWhen.getTime))
    ps.setString(5, sgitCommit.getObjectId.toString.split(" ")(1))
    ps.setString(6, sgitCommit.getMsg)
    val result = ps.executeUpdate

    val rs = ps.getGeneratedKeys
    rs.next match {
      case true => Some(rs.getLong(key(0)).toInt)
      case false => None
    }
  }

//  OK
  def insertSgitAnalysis(gitName: String, date: Date): Option[Int] = {
    val key = Array("analysis_id")
    val query = "insert into analysis(repository_name, analysis_date) values (?, ?);"
    val ps = dbConn.prepareStatement(query, key)
    ps.setString(1, gitName)
    ps.setDate(2, new java.sql.Date(date.getTime()))
    val result = ps.executeUpdate

    val rs = ps.getGeneratedKeys
    rs.next match {
      case true => Some(rs.getLong(key(0)).toInt)
      case false => None
    }
  }

  def selectSgitChangeType: HashMap[String, Int] = {
    val query = "select change_type_id, change_type_name from change_type;"
    val ps = dbConn.prepareStatement(query)
    val rs = ps.executeQuery()

    val map = new HashMap[String, Int]
    while (rs.next) {
      map.put(rs.getString("change_type_name"), rs.getInt("change_type_id"))
    }
    map
  }

  def selectSgitPath: HashMap[String, Int] = {
    val query = "select path_id, path_name from path;"
    val ps = dbConn.prepareStatement(query)
    val rs = ps.executeQuery()

    val map = new HashMap[String, Int]
    while (rs.next) {
      map.put(rs.getString("path_name"), rs.getInt("path_id"))
    }
    map
  }

  def selectSgitFile: HashMap[String, Int] = {
    val query = "select file_id, file_name from file;"
    val ps = dbConn.prepareStatement(query)
    val rs = ps.executeQuery()

    val map = new HashMap[String, Int]
    while (rs.next) {
      map.put(rs.getString("file_name"), rs.getInt("file_id"))
    }
    map
  }

  def close {
    dbConn.close
  }
}