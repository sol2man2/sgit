package org.moringa.oleifera.sgit.model

import java.io.File
import java.io.FileOutputStream
import scala.collection.JavaConverters.asScalaBufferConverter
import org.eclipse.jgit.diff.DiffEntry
import org.eclipse.jgit.diff.DiffFormatter
import org.eclipse.jgit.diff.RawTextComparator
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.util.io.DisabledOutputStream
import scala.collection.mutable.HashMap

object SgitDiffEntry {

  val basePath11 = "base"
  val prevPath = "prev"
  val nextPath = "next"

  def clearDirectory(base: File) {
    def clearRecursive(file: File) {
      if (file.isFile) file.delete
      else if (file.isDirectory) {
        file.listFiles.toList.foreach(clearRecursive)
        file.delete
      } else println("huk<recur>: " + file)
    }
    if (base.isFile) base.delete
    else if (base.isDirectory) base.listFiles.toList.foreach(clearRecursive)
    else println("huk: " + base)
  }

  def deployEntry(repository: Repository, basePath: String, diffEntry: DiffEntry) {

    def deployFile(subPath: String, filePath: String, objectId: ObjectId) {
      try {
        if (!filePath.eq("/dev/null")) {
          val file = new File(basePath + File.separator + subPath, filePath)
          new File(file.getParent).mkdirs
          val fileOutputStream = new FileOutputStream(file)
          repository.open(objectId).copyTo(fileOutputStream)
          fileOutputStream.close
        }
      } catch {
        case e: Exception =>
          println(subPath + " lost" + filePath)
      }
    }

    deployFile(prevPath, diffEntry.getOldPath, diffEntry.getOldId.toObjectId)
    deployFile(nextPath, diffEntry.getNewPath, diffEntry.getNewId.toObjectId)
  }

  def getSgitDiffEntries(repository: Repository, commit: RevCommit, parent: RevCommit): List[SgitDiffEntry] = {

    val basePath = basePath11
    clearDirectory(new File(basePath))

    val diffFormatter = new DiffFormatter(DisabledOutputStream.INSTANCE)
    diffFormatter.setRepository(repository)
    diffFormatter.setDiffComparator(RawTextComparator.DEFAULT)
    diffFormatter.setDetectRenames(true)

    val diffEntries = diffFormatter.scan(parent.getTree, commit.getTree).asScala
    diffEntries.foreach(deployEntry(repository, basePath, _))

    val slocFileName = SgitFileSLOC.execFileSLOC(basePath, prevPath, nextPath)
    val (prevUdbFile, nextUdbFile) = SgitFunctionComplexity.execFunctionComplexity(basePath, prevPath, nextPath)

    //    progress
    val listSLOCs = SgitFileSLOC.getSgitFileSLOCs(basePath, prevPath, nextPath, slocFileName, prevUdbFile, nextUdbFile)

    val sgitDiffEntries = listSLOCs.map( sgitFileSLOC => new SgitDiffEntry(1, "anonymous path", "anonymous file", sgitFileSLOC, Nil))
    sgitDiffEntries
  }

  def getSgitDiffEntry(repository: Repository, basePath: String, prevPath: String, nextPath: String, diffEntry: DiffEntry): SgitDiffEntry = {
    null
  }
}

class SgitDiffEntry(changeType: Int, path: String, file: String, sgitFileSLOC: SgitFileSLOC, functionComplexities: List[SgitFunctionComplexity]) {

  def getChangeType = changeType
  def getPath = path
  def getFile = file
  def getSgitFileSLOC = sgitFileSLOC
  def getFunctionComplexities = functionComplexities

  override def toString = {
    val sb = new StringBuilder
    sb.append("ChangeType: ").append(changeType).
      append(", Path: ").append(path).
      append(", File: ").append(file).append("\n").
      append(", SgitFileSLOC: ").append(sgitFileSLOC)
    sb.toString
  }
}
