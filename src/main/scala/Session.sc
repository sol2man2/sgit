object Session {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val l = List(1,2,3,4,5)                         //> l  : List[Int] = List(1, 2, 3, 4, 5)
  val ml = l.map( x => x*2 )                      //> ml  : List[Int] = List(2, 4, 6, 8, 10)
  
  def f(x: Int) = if (x > 2) Some(x) else None    //> f: (x: Int)Option[Int]
  val fl = l.map(x => f(x))                       //> fl  : List[Option[Int]] = List(None, None, Some(3), Some(4), Some(5))

  def g(v:Int) = List(v-1, v, v+1)                //> g: (v: Int)List[Int]
  val gl = l.map(x => g(x))                       //> gl  : List[List[Int]] = List(List(0, 1, 2), List(1, 2, 3), List(2, 3, 4), Li
                                                  //| st(3, 4, 5), List(4, 5, 6))
  val ffml = l.flatMap(x => f(x))                 //> ffml  : List[Int] = List(3, 4, 5)
  val gfml = l.flatMap(x => g(x))                 //> gfml  : List[Int] = List(0, 1, 2, 1, 2, 3, 2, 3, 4, 3, 4, 5, 4, 5, 6)

  def gx(x: Int) = List( List(List(x), List(x+1), List(x+2)) )
                                                  //> gx: (x: Int)List[List[List[Int]]]
  val gxl = l.map(x => gx(x))                     //> gxl  : List[List[List[List[Int]]]] = List(List(List(List(1), List(2), List(3
                                                  //| ))), List(List(List(2), List(3), List(4))), List(List(List(3), List(4), List
                                                  //| (5))), List(List(List(4), List(5), List(6))), List(List(List(5), List(6), Li
                                                  //| st(7))))
  val gxfml = l.flatMap(x => gx(x))               //> gxfml  : List[List[List[Int]]] = List(List(List(1), List(2), List(3)), List(
                                                  //| List(2), List(3), List(4)), List(List(3), List(4), List(5)), List(List(4), L
                                                  //| ist(5), List(6)), List(List(5), List(6), List(7)))
  val map = Map(1 -> 2, 2 -> 4, 3 -> 6)           //> map  : scala.collection.immutable.Map[Int,Int] = Map(1 -> 2, 2 -> 4, 3 -> 6)
                                                  //| 
  val mapl = map.toList                           //> mapl  : List[(Int, Int)] = List((1,2), (2,4), (3,6))
  map.mapValues(v => v*2)                         //> res0: scala.collection.immutable.Map[Int,Int] = Map(1 -> 4, 2 -> 8, 3 -> 12)
                                                  //| 
  map.mapValues(v => f(v))                        //> res1: scala.collection.immutable.Map[Int,Option[Int]] = Map(1 -> None, 2 -> 
                                                  //| Some(4), 3 -> Some(6))
  map.map(e => List(e._2))                        //> res2: scala.collection.immutable.Iterable[List[Int]] = List(List(2), List(4)
                                                  //| , List(6))
  map.map(e => e._2)                              //> res3: scala.collection.immutable.Iterable[Int] = List(2, 4, 6)
  
  map.flatMap(e => List(e._2))                    //> res4: scala.collection.immutable.Iterable[Int] = List(2, 4, 6)
  map.map(e => e)                                 //> res5: scala.collection.immutable.Map[Int,Int] = Map(1 -> 2, 2 -> 4, 3 -> 6)
                                                  //| 
  map.flatMap(e => List(e))                       //> res6: scala.collection.immutable.Map[Int,Int] = Map(1 -> 2, 2 -> 4, 3 -> 6)
                                                  //| 
  map.flatMap(e => f(e._2))                       //> res7: scala.collection.immutable.Iterable[Int] = List(4, 6)

  def h(k:Int, v:Int) = if (v > 2) Some(k->v) else None
                                                  //> h: (k: Int, v: Int)Option[(Int, Int)]
  map.flatMap ( e => h(e._1,e._2) )               //> res8: scala.collection.immutable.Map[Int,Int] = Map(2 -> 4, 3 -> 6)
  map.flatMap { case (k,v) => h(k,v) }            //> res9: scala.collection.immutable.Map[Int,Int] = Map(2 -> 4, 3 -> 6)
}