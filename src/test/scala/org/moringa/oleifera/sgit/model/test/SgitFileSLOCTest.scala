package org.moringa.oleifera.sgit.model.test

import scala.io._
import scala.sys.process.stringToProcess
import org.moringa.oleifera.sgit.model.SgitFileSLOC
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import org.moringa.oleifera.sgit.model.SgitChangeType
import scala.collection.mutable.HashMap

class SgitFileSLOCTest extends FunSuite with BeforeAndAfter {

  before {
  }
  
  ignore("1: run AgileWBS object") {
    val basePath = """base"""
    val prevPath = "prev"
    val nextPath = "next"

    //    val builder = new FileRepositoryBuilder
    //    val repository = builder.setGitDir(new File(repositoryName)).readEnvironment().findGitDir().build()
    SgitFileSLOC.getSgitFileSLOCs(basePath, prevPath, nextPath, null, null, null)
    val result = "ucc -d -dir C:\\zProject\\scala\\sgit\\base\\prev C:\\zProject\\scala\\sgit\\base\\next *.h *.hpp *.c *.cpp -outdir base" !

    println("result: " + result)

    val resultFile = "C:\\zProject\\scala\\sgit\\base\\outfile_diff_results.csv"
    val source = scala.io.Source.fromFile(resultFile)
    val lines = source.getLines.toList.drop(8).takeWhile( _ != "")

    def getSLOC(diffLines: String): SLOC = {
      val diff = diffLines.split(",")
      val add: Int = diff(0).toInt
      val mod: Int = diff(2).toInt
      val del: Int = diff(1).toInt
      val untouch: Int = diff(3).toInt
      val changeType = SgitChangeType.getSgitChangeTypeFromStr(diff(4))
      
      val prevBase = "base\\prev\\"
      val nextBase = "base\\next\\"

      val prevFile = diff(6).tail.init
      val nextFile = diff(7).tail.init

      val prevPath = prevFile.lastIndexOf(prevBase) match {
        case -1 => prevFile.toString
        case index => prevFile.splitAt(index + prevBase.length)._2.toString
      }

      val nextPath = nextFile.lastIndexOf(nextBase) match {
        case -1 => nextFile.toString
        case index => nextFile.splitAt(index + nextBase.length)._2.toString
      }

      val file: String = changeType match {
        case add if add == SgitChangeType.Add => nextPath
        case mod if mod == SgitChangeType.Modify => nextPath
        case del if del == SgitChangeType.Delete => prevPath
        case _ => throw new Exception("err")
      }

      new SLOC(add, mod, del, untouch, diff(4), file)
    }

    val re = lines.map(getSLOC)
    val map = new HashMap[String, SLOC]
    re.foreach( elem => {
      map(elem.getFile) = elem
    })
    
    map.keys.foreach( elem => {
      println(elem + ":" + map(elem))
    })

    source.close

    assert(true)
  }

  test("2: calc sloc") {
    val basePath = """base"""
    val prevPath = "prev"
    val nextPath = "next"

    val lines = SgitFileSLOC.execFileSLOC(basePath, prevPath, nextPath)
    println(lines)
  }

  class SLOC(add: Int, mod: Int, del: Int, untouch: Int, changeType: String, file: String) {
    def getAdd = add
    def getFile = file
    override def toString = "[" + file + "] " + "add: " + add + ", mod: " + mod + ", del: " + del + ", untouch: " + untouch 
  }

  after {
  }

}