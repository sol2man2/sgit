package org.moringa.oleifera.sgit.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import sys.process._
import org.moringa.oleifera.sgit.model.DataAgent
import scala.collection.mutable.HashMap

class AppMainTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("1: run AgileWBS object") {
    val result = "uperl sgit.pl" !
    
    println(result)
    
    assert(true)
  }

  test("2: ") {
    val p = """([\w:_]+)-(\d+)""".r
//    val line = "ApplicationManager::generateNewTicket-1"
    val line = "TestAppMgrService::setParentlResponseObj-1"
    val p(func, complexity) = line
    println("f: " + func + "c: " + complexity)
//    line match {
//      case """(\w+)-(\d+)""" => println(line)
//      case _ => println("not matched");
//    }
  }
  
  test("3: ") {
    val serverName = "localhost"
    val dbName = "sgit"

    val dataAgent = new DataAgent(serverName, dbName)
    
    val pMap = new HashMap[String, Int]
    val nMap = new HashMap[String, Int]
    
    pMap += (("foo", 1))
    pMap += (("bar", 2))
    
    nMap += (("foo", 10))
    nMap += (("hoo", 30))
    
    dataAgent.insertSgitCommitComplexity(1, "huk", Some(1), Some(2))
  }

  after {
  }

}
