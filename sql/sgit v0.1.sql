﻿--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	table creation

-- Table: analysis

-- DROP TABLE analysis;

CREATE TABLE analysis
(
  analysis_id serial NOT NULL,
  repository_name character varying,
  analysis_date date
)
WITH (
  OIDS=FALSE
);
ALTER TABLE analysis
  OWNER TO agile;

--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	table creation

-- Table: change_type

-- DROP TABLE change_type;

CREATE TABLE change_type
(
  change_type_id serial NOT NULL,
  change_type_name character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE change_type
  OWNER TO agile;


--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	table creation

-- Table: commit

-- DROP TABLE commit;

CREATE TABLE commit
(
  commit_id serial NOT NULL,
  commit_object_id character varying,
  analysis_id integer,
  author character varying,
  committer character varying,
  commit_date date,
  commit_msg character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE commit
  OWNER TO agile;

--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	table creation

-- Table: diff_entry

-- DROP TABLE diff_entry;

CREATE TABLE diff_entry
(
  diff_entry_id serial NOT NULL,
  commit_id integer,
  change_type_id integer,
  path_id integer,
  file_id integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE diff_entry
  OWNER TO agile;


--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	table creation

-- Table: file

-- DROP TABLE file;

CREATE TABLE file
(
  file_id serial NOT NULL,
  file_name character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE file
  OWNER TO agile;

--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	table creation

-- Table: path

-- DROP TABLE path;

CREATE TABLE path
(
  path_id serial NOT NULL,
  path_name character varying
)
WITH (
  OIDS=FALSE
);
ALTER TABLE path
  OWNER TO agile;



select * from analysis;
select * from change_type;
select * from path;
select * from file;
select * from commit;
select * from diff_entry;

select path_id from path where path_name like '/%';

select 
	* 
from 
	diff_entry 
where 
	path_id in (select path_id from path where path_name not like '/%')
;

--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	all records except '/dev' and !'*.java'
select
	a_table.analysis_id, a_table.repository_name, a_table.analysis_date,
	c_table.author, c_table.committer, c_table.commit_date, c_table.commit_object_id, c_table.commit_msg,
	ct_table.change_type_name, 
	p_table.path_name,
	f_table.file_name
from 
	diff_entry as de_table
	left outer join commit as c_table
	on de_table.commit_id = c_table.commit_id
	left outer join analysis as a_table
	on c_table.analysis_id = a_table.analysis_id
	left outer join change_type as ct_table
	on de_table.change_type_id = ct_table.change_type_id
	left outer join path as p_table
	on de_table.path_id = p_table.path_id
	left outer join file as f_table
	on de_table.file_id = f_table.file_id
where
	f_table.file_name like '%.java'
	and p_table.path_name not like '/%'
;


--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	all records with group by folder except '/dev' and !'*.java'

select
	a_table.repository_name, a_table.analysis_date,
	c_table.author, c_table.committer, c_table.commit_date, c_table.commit_object_id, c_table.commit_msg,
	ct_table.change_type_name, 
	p_table.path_name
from 
	diff_entry as de_table
	left outer join commit as c_table
	on de_table.commit_id = c_table.commit_id
	left outer join analysis as a_table
	on c_table.analysis_id = a_table.analysis_id
	left outer join change_type as ct_table
	on de_table.change_type_id = ct_table.change_type_id
	left outer join path as p_table
	on de_table.path_id = p_table.path_id
	left outer join file as f_table
	on de_table.file_id = f_table.file_id
where
	f_table.file_name like '%.java'
	and p_table.path_name not like '/%'
group by
	p_table.path_name, a_table.repository_name, a_table.analysis_date,
	c_table.author, c_table.committer, c_table.commit_date, c_table.commit_object_id, c_table.commit_msg,
	ct_table.change_type_name
;



--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	commit # in file, author, committer
select
	concat(p_t.path_name, f_t.file_name) as file,
	c_t.author, c_t.committer,
	count(*)
from
	diff_entry as de_t
	left outer join commit as c_t
	on de_t.commit_id = c_t.commit_id
	left outer join path as p_t
	on de_t.path_id = p_t.path_id
	left outer join file as f_t
	on de_t.file_id = f_t.file_id
group by file, c_t.author, c_t.committer
;

--	----------------------------------------------------------------------------------------------
--	commit # in file, author
select
	concat(p_t.path_name, f_t.file_name) as file,
	c_t.author,
	count(*)
from
	diff_entry as de_t
	left outer join commit as c_t
	on de_t.commit_id = c_t.commit_id
	left outer join path as p_t
	on de_t.path_id = p_t.path_id
	left outer join file as f_t
	on de_t.file_id = f_t.file_id
group by file, c_t.author
;


--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	test git
select
	a_table.analysis_id, a_table.repository_name, a_table.analysis_date,
	c_table.author, c_table.committer, c_table.commit_date, c_table.commit_object_id, c_table.commit_msg,
	ct_table.change_type_name, 
	p_table.path_name,
	f_table.file_name
from 
	diff_entry as de_table
	left outer join commit as c_table
	on de_table.commit_id = c_table.commit_id
	left outer join analysis as a_table
	on c_table.analysis_id = a_table.analysis_id
	left outer join change_type as ct_table
	on de_table.change_type_id = ct_table.change_type_id
	left outer join path as p_table
	on de_table.path_id = p_table.path_id
	left outer join file as f_table
	on de_table.file_id = f_table.file_id
where
	p_table.path_name not like '/%'
	and c_table.commit_object_id = 'cdacaa92c476b1fecf1504d1f253f716c533aeab'
;

--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	author and committer name when author and committer are different
select
	author, committer
from
	commit
where
	author <> committer
;
--	conclusion: Gerrit Code Review only commit which has 2 parents
--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------

	
--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	commit contents.
select
	*
from commit
where
	commit_msg like '%merge %'
;

	
--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	kind of folders
select
	distinct path_name
from path
where
	path_name like 'src/%'
;

	
--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	package according ot commit

--	------------------------------------------------------
--	view commit's contents.
select
	a_table.repository_name, a_table.analysis_date,
	c_table.commit_id, c_table.commit_date, c_table.commit_msg,
	c_table.author,
	p_table.path_id, p_table.path_name
from 
	commit as c_table
	left outer join analysis as a_table
	on c_table.analysis_id = a_table.analysis_id
	left outer join diff_entry as de_table
	on c_table.commit_id = de_table.commit_id
	left outer join change_type as ct_table
	on de_table.change_type_id = ct_table.change_type_id
	left outer join path as p_table
	on de_table.path_id = p_table.path_id
	left outer join file as f_table
	on de_table.file_id = f_table.file_id
where
	f_table.file_name like '%.java'
	and p_table.path_name not like '/%'
	and c_table.commit_date > '2013-06-06' and c_table.commit_date < '2013-07-31' 
--	and ct_table.change_type_name in ('MODIFY', 'ADD')	--	'RENAME', 'DELETE', 'COPY'
group by 
	a_table.repository_name, a_table.analysis_date,
	c_table.commit_id, c_table.author, c_table.commit_date, c_table.commit_msg,
	p_table.path_id, p_table.path_name
;

select distinct(author) from commit;
select * from change_type;
select * from commit where commit_date > '2013-06-06' and commit_date < '2013-07-31' 
select * from path where path_id = 2624;
select * from path where path_id = 2541;

select
	a_table.analysis_id, a_table.repository_name, a_table.analysis_date,
	c_table.author, c_table.committer, c_table.commit_date, c_table.commit_object_id, c_table.commit_msg,
	ct_table.change_type_name, 
	p_table.path_name,
	f_table.file_name
;

--	------------------------------------------------------
--	check commit_id 320's contents.
select 
	* 
from 
	commit as c 
	right outer join diff_entry as de 
	on c.commit_id = de.commit_id  
	left outer join path as p
	on de.path_id = p.path_id
	left outer join file as f
	on de.file_id = f.file_id
where 
	c.commit_id=320
	and f.file_name like '%.java'
;










--	----------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	delete all records

delete from analysis;
delete from change_type;
delete from path;
delete from file;
delete from commit;
delete from diff_entry;

