
#
# Sample Understand perl API program 
#
# Synopsis: Reports all project files 
#
# Categories: Project Report
#
# Languages: All
#
# Usage:
sub usage($)
{
    return @_[0] . << "END_USAGE";
Usage: $0
    Displays the info. browser entity for the currently open instance of Understand.
END_USAGE
}

#
#  For the latest Understand perl API documentation, see 
#      http://www.scitools.com/perl/
#  Refer to the documenation for details on using the perl API 
#  with Understand and for details on all Understand perl API calls.
# 
#  12-10-01 Evan Knop
#
use Understand;
use strict;

system("cls");

#my $ver = Understand::version();
#print "Understand Ver.: ", $ver, "\n";

#my ($db, $status) = Understand::open("sam.udb");
# my ($db, $status) = Understand::open("prev/prev.udb");
# my ($db, $status) = Understand::open("base/next/next.udb");

my $exename = $ARGV[0];
# print "db name: ", $exename, "\n";

my ($db, $status) = Understand::open("base/$exename.udb");
die "Error status: ", $status, "\n" if $status;

sub print_entity() {
	foreach my $ent ($db->ents()) {
		# print entity and its kind
		print $ent->name()," [",$ent->kindname(),"]\n";
	}
}

sub print_file() {
	foreach my $file ($db->ents("File")) {
		# print the long name including directory names
		print $file->longname(),"\n";
	}
}

sub print_lookup() {
	foreach my $file ($db->lookup("*.cpp","File")) {
		print $file->name(),"\n";
	}
}

#print_entity();
#print_file();
#print_lookup();

sub print_get_entity_attribute() {
	foreach my $func ($db->ents("Function")) {
		print $func->longname(),"(";
		my $first = 1;
		# get list of refs that define a parameter entity
		foreach my $param ($func->ents("Define","Parameter")) {
			print ", " unless $first;
			print $param->type()," ",$param->name;
			$first = 0;
		}
		print ")\n";
	}
}

#print_get_entity_attribute();

sub tmp() {
 # loop through all project metrics
  foreach my $met ($db->metrics()) {
    print $met," = ",$db->metric($met),"\n";
  }
}

sub test() {
	my @metrics = $db->metrics();
	foreach my $metric (@metrics) {
		print $metric, "\n";
	}
}
# test();

sub get_function_complexity() {
	foreach my $func ($db->ents("Function")) {
		print "[";
		print $func->longname();
		print "] ";
		print $func->metric("Cyclomatic");
		print "\n";
	}	
}
# get_function_complexity();

sub get_file_info() {
	foreach my $file ($db->ents("File")) {
		print $file->longname(),"\n";

	}
}
# get_file_info();


sub get_c_kind() {
	foreach my $c_kind ($db->ents("C Class Type")) {
		print $c_kind->longname,"\n";

	}
}
# get_c_kind();


sub get_array_function_complexity() {
	my @complexities;
	my $sum = 0;
	my $count = 0;
	foreach my $func ($db->ents("Function")) {

		my $complexity = $func->metric("Cyclomatic");
		if( !defined( $complexity ) ) {
			next;
		}
		print $func->longname(), "-", "$complexity", "\n";
		push @complexities, $complexity;
		$sum += $complexity;
		$count += 1;
	}
	# my $array_size = @complexities;
	# print $array_size;
	# print "\n";
	# print "size: ", $count, "\n";
	# print "sum: ", $sum, "\n";

	# my $first = 1;

	# my $file = "./base/$exename.cc";
	# # print "output file: ", $file;

	# open(TXT, ">$file");
	# foreach my $com (@complexities) {
	# 	if($first == 1) {
	# 		$first = 0;
	# 	} else {
	# 		print TXT ",";
	# 	}
	# 	print TXT $com;
	# }

	# close TXT
}

get_array_function_complexity();

$db->close();
# print "##	finish\n";
# print "##################\n";
